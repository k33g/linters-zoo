# by @k33g
FROM alpine:latest

RUN apk --update add --no-cache bash wget curl tar git jq util-linux \
    nodejs=12.14.0-r0 npm=12.14.0-r0 \
    openjdk11=11.0.5_p10-r0 \
    maven=3.6.3-r0

WORKDIR /
RUN curl -L https://github.com/checkstyle/checkstyle/releases/download/checkstyle-8.27/checkstyle-8.27-all.jar -o checkstyle.jar

RUN npm install -g eslint
RUN npm install -g xml-js


